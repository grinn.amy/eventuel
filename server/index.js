require('dotenv').config();
const init = require('./src/models/init').default;
const app = require('./src/app').default;

const { PORT } = process.env;

init().then(() =>
  app.listen(PORT, () => console.log(`App listening on port ${PORT}`))
);
