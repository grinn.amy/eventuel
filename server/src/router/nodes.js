const path = require('path');
const { Router } = require('express');
const { mainAuth, nodeAuth } = require('../util/auth');
const { MalformedInputError } = require('../util/errors');
const sse = require('../util/sse').default;
const nodes = require('../models/nodes');
const users = require('../models/users');
const usersRouter = require('./users').default;
const log = require('../log').default;

const { INDEX_NODES } = process.env;

const router = Router();
exports.default = router;

router.use('/:node/users', usersRouter);

if (INDEX_NODES === 'true') {
  router.get('/', async (_, res, next) => {
    try {
      return res.status(200).json(await nodes.getAll());
    } catch (err) {
      return next(err);
    }
  });
} else {
  router.get('/', mainAuth, async (_, res, next) => {
    try {
      return res.status(200).json(await nodes.getAll());
    } catch (err) {
      return next(err);
    }
  });
}

router.post('/', mainAuth, async (req, res, next) => {
  const node = req.body;

  if (
    !node.name ||
    !node.tags ||
    node.public ||
    !node.users ||
    (node.users && node.users.find((user) => !user.user || !user.pass))
  ) {
    return next(new MalformedInputError());
  }

  try {
    await nodes.create(node);
    if (node.users) {
      await users.createAll(node.name, node.users);
    }
  } catch (err) {
    return next(err);
  }
  return res.sendStatus(200);
});

router.delete('/:node', mainAuth, async (req, res, next) => {
  try {
    await nodes.remove(req.params.node);
  } catch (err) {
    return next(err);
  }
  return res.sendStatus(200);
});

router.get(
  '/:node/stream',
  nodeAuth,
  sse(async (conn, req) => {
    const tags = await nodes.getTags(req.params.node);
    const listener = (entry) => {
      const intersection = tags.filter((tag) => entry.tags.includes(tag));
      if (intersection.length > 0) {
        conn.send(entry);
      }
    };
    log.on('entry', listener);
    conn.onClose(() => log.off('entry', listener));
  })
);

router.get('/:node', nodeAuth, (req, res) => {
  res.status(200).sendFile(path.resolve(__dirname, '..', 'web', 'node.html'));
});
