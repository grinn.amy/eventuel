const bcrypt = require('bcrypt');
const { sql } = require('./sql');
const { ConflictError } = require('../util/errors');

exports.init = () => sql`
  CREATE TABLE IF NOT EXISTS users (
    user TEXT NOT NULL,
    pass TEXT NOT NULL,
    node TEXT NOT NULL
  )
`;

exports.exists = async (node, user) =>
  (
    await sql`
      SELECT COUNT(*) as count
      FROM users
      WHERE node = ${node} AND user = ${user}
    `
  )[0].count;

exports.createAll = async (node, users) => {
  for (const user of users) {
    if (await exports.exists(node, user.user)) {
      throw new ConflictError(
        `A user with the name "${user.user}" already exists in node "${node}"`
      );
    }
  }

  for (const user of users) {
    user.pass = await bcrypt.hash(user.pass, 10);
  }

  return Promise.all(
    users.map(
      (user) => sql`
        INSERT INTO users (user, pass, node)
        VALUES (${user.user}, ${user.pass}, ${node})
      `
    )
  );
};

exports.getHash = async (node, user) =>
  (
    await sql`
      SELECT pass
      FROM users
      WHERE node = ${node} AND user = ${user}
    `
  )[0].pass;

exports.getAll = async (node) =>
  (
    await sql`
      SELECT user
      FROM users
      WHERE node = ${node}
    `
  ).map(({ user }) => user);

exports.remove = (node, user) => sql`
  DELETE FROM users
  WHERE node = ${node} AND user = ${user}
`;

exports.removeAll = (node) => sql`
  DELETE FROM users
  WHERE node = ${node}
`;

exports.setPassword = async (node, user, pass) => {
  const hash = await bcrypt.hash(pass, 10);
  return sql`
    UPDATE users
    SET pass = ${hash}
    WHERE node = ${node} AND user = ${user}
  `;
};
