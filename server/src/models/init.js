const nodes = require('./nodes');
const users = require('./users');

exports.default = () => Promise.all([nodes.init(), users.init()]);
