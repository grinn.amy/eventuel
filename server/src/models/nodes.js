const { sql } = require('./sql');
const users = require('./users');

exports.init = () => sql`
  CREATE TABLE IF NOT EXISTS nodes (
    name TEXT NOT NULL UNIQUE,
    tags TEXT,
    public INTEGER,
    lastClockIn TEXT
  )
`;

exports.create = (node) => {
  const tags = node.tags.split(/\s+|(?:,\s*)/).join(' ');

  return sql`
    INSERT INTO nodes (name, tags, public)
    VALUES (${node.name}, ${tags}, ${node.public})
  `;
};

exports.getTags = async (name) =>
  (
    await sql`
      SELECT tags
      FROM nodes
      WHERE name = ${name}
    `
  )[0].tags.split(' ');

exports.isPublic = async (name) =>
  (
    await sql`
      SELECT public
      FROM nodes
      WHERE name = ${name}
    `
  )[0].public;

exports.getAll = async () =>
  (
    await sql`
      SELECT name
      FROM nodes
    `
  ).map(({ name: node }) => node);

exports.remove = (name) =>
  Promise.all([
    sql`
      DELETE FROM nodes
      WHERE name = ${name}
    `,
    users.removeAll(name),
  ]);
